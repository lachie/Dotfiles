# Dotfiles ✨
my dotfiles 🛠 💻

### Configs for
- 🐟  fish shell
- 🖥  iterm2
- ⛓  tmux
- 📝  vim

### Git emoji glossary
Emojis are used in the prompt to display the git status of the current directory.

| emoji | description|
|-------|------------|
|🚀     | Clean State  |
|⚡️     | Unclean State |
|⚗️     | Staged State |
|🔥🚨   | Conflicted State |
|🚜     | Untracked Files |
|👆🏻     | Upstream ahead |
|👇🏻     | Upstream behind|

### Screenshots
![default](https://raw.githubusercontent.com/alachie/Dotfiles/master/screenshots/1.jpg)
